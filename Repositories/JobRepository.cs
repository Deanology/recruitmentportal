﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class JobRepository: IJobRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public JobRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public string SaveJob(JobViewModel model)
        {

            if (model.Id == null)
            {
                var job = _mapper.Map<JobViewModel, Job>(model);
                job.Audit = new Audit();
                _context.Jobs.Add(job);
                Save();

                return "Created";
            }

           
            
            var jobDb = GetJob(model.Id.Value);
            jobDb = _mapper.Map(model, jobDb);
            jobDb.Audit.UpdatedAt = DateTime.Now;
            _context.Jobs.Update(jobDb);
        

            Save();
            return "Updated";
        }

        public IEnumerable<Job> GetJobs()
        {
            return _context.Jobs
                .Include(j => j.Applications)
                .Include(j => j.Type)
                .Include(j => j.Employer)
                .ThenInclude(j => j.Logo)
                .Include(j => j.Employer)
                .ThenInclude(j => j.Industry)
                .ToList();
        }

        public IEnumerable<Industry> GetIndustries()
        {
            return  _context.Industries.ToList();
            
        }

        public Job GetJob(int id)
        {
            return _context.Jobs
                .Include(j => j.Applications)
                .Include(j => j.Type)
                .Include(j => j.Employer)
                .ThenInclude(j => j.Logo).
                Include(j => j.Employer)
                .ThenInclude(j => j.Industry)

                .SingleOrDefault(j => j.Id == id);
        }
        public Job GetJobFor(int id, string userId)
        {
            return _context.Jobs
                .Include(j => j.Applications)
                .Include(j => j.Type)
                .Include(j => j.Employer)
                .ThenInclude(j => j.Logo)
                .Include(j => j.Employer)
                .ThenInclude(j => j.Industry)
                .SingleOrDefault(j => j.Id == id && j.EmployerId == userId);
        }
        public IEnumerable<Job> GetJobsFor(string userId)
        {
            return 
                GetJobs()
                    .Where(j => j.EmployerId == userId)
                    .ToList();
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}