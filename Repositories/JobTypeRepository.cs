﻿using System.Collections.Generic;
using System.Linq;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class JobTypeRepository : IJobTypeRepository
    {
        private readonly ApplicationDbContext _context;

        public JobTypeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<JobType> GetJobTypes()
        {
            return _context.JobTypes.ToList();
        }
    }
}