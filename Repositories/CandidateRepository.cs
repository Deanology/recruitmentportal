﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Services;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class CandidateRepository : ICandidateRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CandidateRepository( ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    
        public void Create( string id, RegisterCandidateViewModel model )
        {

            var candidate = new Candidate
            {
                CandidateId = id,
                Fullname = $"{model.FirstName} {model.LastName}",
                GenderId = model.GenderId,
                TermsCondition = model.TermsConditions,
                Audit = new Audit()

            };
            _context.Candidates.Add(candidate);
        }

        public bool Update(CandidateProfileViewModel model, IFormFile photo = null)
        {
       

            var candidateInDb = GetCandidate(model.CandidateId);
            if (model.Photo == null && model.CandidateId != null)
            {
                model.Photo = candidateInDb.Photo;
            }

            if (photo != null)
            {
                model.Photo = UpsertFile.Save(model.Photo, FileType.Photo, photo);
            }

            var candidate = _mapper.Map(model, candidateInDb);

        
            candidate.Audit.UpdatedAt = DateTime.Now;

            _context.Candidates.Update(candidate);
            Save();
            return true;
            
        }


        public IEnumerable<Candidate> GetCandidates()
        {
            return _context.Candidates.ToList();
        }

        public Candidate GetCandidate(string id)
        {
            return _context.Candidates.AsQueryable()
                .Include( c => c.Photo)
                .Include(c => c.EducationLevel)
                .Include(c => c.Gender)
                .SingleOrDefault(c => c.CandidateId == id);

        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}