﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Services;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class EmployerRepository : IEmployerRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public EmployerRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Create(string id, RegisterEmployerViewModel model)
        {
            var employer = new Employer
            {
                EmployerId = id,
                CompanyName = model.CompanyName,
                Capacity = model.Capacity,
                IndustryId = model.IndustryId,
                TermsCondition = model.TermsConditions,
                Audit = new Audit()


            };

            _context.Employers.Add(employer);
        }

        public bool Update(EmployerProfileViewModel model, IFormFile logo = null)
        {

            var employerInDb = GetEmployer(model.EmployerId);
            if (model.Logo == null && model.EmployerId != null)
            {
                model.Logo = employerInDb.Logo;
            }

            if (logo != null)
            {
                model.Logo = UpsertFile.Save(model.Logo, FileType.Photo, logo);
            }

            var employer = _mapper.Map(model, employerInDb);


            employer.Audit.UpdatedAt = DateTime.Now;

            _context.Employers.Update(employer);
            Save();
            return true;

        }


        public IEnumerable<Employer> GetEmployers()
        {
            return _context.Employers.ToList();
        }

        public Employer GetEmployer(string id)
        {
           return _context.Employers
               .Include(e => e.Industry)
               .Include(e => e.Logo)
                .Include(e => e.Jobs)
                .SingleOrDefault(e => e.EmployerId == id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}