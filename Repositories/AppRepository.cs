﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Services;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class AppRepository: IAppRepository
    {
        private readonly ApplicationDbContext _context;

        public AppRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string UpSert(ApplicationViewModel model, string userId, IFormFile resume = null)
        {
            if (model.AppId == 0)
            {
               
                if (resume != null)
                {
                    model.Resume = UpsertFile.Save(model.Resume, FileType.Resume, resume);
                }
                var appForm = new Application
                {
                    CandidateId = model.CandidateId,
                    JobId = model.JobId,
                    FullName = model.FullName,
                    Resume = model.Resume,
                    PhoneNumber = model.PhoneNumber,
                    TermsConditions = model.TermsConditions,
                    Audit = new Audit()
                };
                _context.Applications.Add(appForm);
                Save();
                return "Created";
            }

            var appFormDb = _context.Applications
                .Include(a => a.Resume)
                .SingleOrDefault(a => a.Id == model.AppId && a.CandidateId == userId);

            if (appFormDb != null )
            {
                model.Resume = appFormDb.Resume;
                if (resume != null)
                {
                    appFormDb.Resume = UpsertFile.Save(model.Resume, FileType.Resume, resume);
                }

                appFormDb.FullName = model.FullName;
                appFormDb.PhoneNumber = model.PhoneNumber;
                  
                _context.Applications.Update(appFormDb);
                Save();
                return "Updated";
            }

            return "SomeError";

        }

        public IEnumerable<Application> GetApplicationsForEmployer(string employerId)
        {
            return _context.Applications
                .Include(a => a.Job)
                .Include(a => a.Candidate)
                .Include(a => a.Resume)
                .Where(a => a.Job.EmployerId == employerId)
                .ToList();
        }

        public IEnumerable<Application> GetApplicationsForCandidate(string candidateId)
        {
            return _context.Applications
                .Include(a => a.Job)
                .ThenInclude(a => a.Type)
                .Include(a => a.Candidate)
                .Include(a => a.Job)
                .ThenInclude(a => a.Employer)
                .Where(a => a.CandidateId == candidateId)
                .ToList();
        }
        public Application GetApplicationForCandidate(string userId, int id)
        {
            return GetApplicationsForCandidate(userId).SingleOrDefault(a => a.Id == id);
        }

        public Application GetApplicationForEmployer(string userId, int id)
        {
            return GetApplicationsForEmployer(userId).SingleOrDefault(a => a.Id == id);
        }

       

        public void Mark(string userId, bool value, int id)
        {
            var application = GetApplicationForEmployer(userId, id);
            application.Approve = value;
            application.Audit.UpdatedAt = DateTime.Now;
            _context.Applications.Update(application);
            Save();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public IEnumerable<Application> GetApplications()
        {
            return _context.Applications.
                Include(a => a.Job)
                .ThenInclude(a => a.Type)
                .ToList();
        }
    }
}