﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Profiles
{
    public class CandidateProfile : Profile
    {

        public CandidateProfile()
        {
            CreateMap<Candidate, CandidateProfileViewModel>();
            CreateMap<CandidateProfileViewModel, Candidate>()
                .ForMember(_ => _.Audit, opt => opt.Ignore())
                .ForMember(_ => _.Fullname, opt => opt.Ignore())
                .ForMember(_ => _.GenderId, opt => opt.Ignore());

        }
    }
}
