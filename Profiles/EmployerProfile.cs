﻿using AutoMapper;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Profiles
{
    public class EmployerProfile : Profile
    {

        public EmployerProfile()
        {
            CreateMap<Employer, EmployerProfileViewModel>();
            CreateMap<EmployerProfileViewModel, Employer>()
                .ForMember(_ => _.Audit, opt => opt.Ignore())
                .ForMember(_ => _.CompanyName, opt => opt.Ignore())
                .ForMember(_ => _.IndustryId, opt => opt.Ignore());

        }
    }
}