using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Services
{
    public static class SeedAppData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            var context = app.ApplicationServices
                .CreateScope().ServiceProvider.GetRequiredService<ApplicationDbContext>();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

           
           
            if (!context.JobTypes.Any())
            {
                context.JobTypes.AddRange(

                    new JobType
                    {
                        Type = "Full Time",
                        Audit = new Audit()
                    },
                    new JobType
                    {
                        Type = "Part Time",
                        Audit = new Audit()
                    },
                    new JobType
                    {
                        Type = "Contract",
                        Audit = new Audit()
                    }

                );
            }

            if (!context.Genders.Any())
            {
                context.Genders.AddRange(
                    new Gender
                    {
                        Name = "Male",
                        Audit = new Audit()
                    },

                    new Gender
                    {
                        Name = "Female",
                        Audit = new Audit(),
                    },
                    new Gender
                    {
                        Name = "Prefer not to Specify",
                        Audit = new Audit()
                    }
                );
            }

            if (!context.Industries.Any())
            {
                context.Industries.AddRange(IndustryData.Data());
            }

            if (!context.EducationLevels.Any())
            {
                context.EducationLevels.AddRange(
                    new EducationLevel
                    {
                        Level = "Diploma",
                        Audit = new Audit()
                    },

                    new EducationLevel
                    {
                        Level = "Degree",
                        Audit = new Audit(),
                    },

                    new EducationLevel
                    {
                        Level = "Masters",
                        Audit = new Audit(),
                    },
                    new EducationLevel
                    {
                        Level = "PhD",
                        Audit = new Audit(),
                    },
                    new EducationLevel
                    {
                        Level = "Professor",
                        Audit = new Audit(),
                    }
                );
            }

            context.SaveChanges();

        }

    }
}