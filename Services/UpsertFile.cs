using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Recruitement_App___BEZAO_Test.Models;
using File = Recruitement_App___BEZAO_Test.Models.File;

namespace Recruitement_App___BEZAO_Test.Services
{
    public class UpsertFile
    {
        public static File Save(File file, FileType type, IFormFile media = null)
        {
            if (file != null)
            {
              file.Name = Path.GetFileName(media.FileName);
              file.Type = type;
              file.ContentType = media.ContentType;
                using (var reader = new MemoryStream())
                {
                    media.CopyTo(reader);
                    file.Content = reader.ToArray();
                    file.Audit.UpdatedAt = DateTime.Now;
                    return file;
                }
            }
            else
            {
                file = new File()
                {
                    Name = Path.GetFileName(media.FileName),
                    Type = type,
                    ContentType = media.ContentType,
                    Audit = new Audit()
                };
                using (var reader = new MemoryStream())
                {
                    media.CopyTo(reader);

                    file.Content = reader.ToArray();
                    return file;
                }

            }
        }
    }
}