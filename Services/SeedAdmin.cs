using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Recruitement_App___BEZAO_Test.Areas.Identity.Data;

namespace Recruitement_App___BEZAO_Test.Services
{
    public static class SeedAdmin
    {
        private const string AdminUser = "Admin";
        private const string AdminPassword = "Secret@123$";

        public static async void EnsurePopulated(IApplicationBuilder app)
        {
            var context = app.ApplicationServices
                .CreateScope().ServiceProvider
                .GetRequiredService<IdentityContext>();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            var userManager = app.ApplicationServices
                .CreateScope().ServiceProvider
                .GetRequiredService<UserManager<IdentityUser>>();
            var user = await userManager.FindByIdAsync(AdminUser);
            if (user != null) return;
            user = new IdentityUser("Admin")
                {Email = "admin@domain.com", PhoneNumber = "090-123-4090", EmailConfirmed = true};
            var createUser = await userManager.CreateAsync(user, AdminPassword);

            if (createUser.Succeeded)
            {
                await userManager.AddToRoleAsync(user, "Admin");
            }
        }
    }
}