﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IEmployerRepository
    {
        void Create(string id, RegisterEmployerViewModel model);
        bool Update(EmployerProfileViewModel model, IFormFile logo = null);
        IEnumerable<Employer> GetEmployers();
        Employer GetEmployer(string id);

        void Save();


    }
}

