﻿namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IUnitOfWork
    {
        ICandidateRepository CandidateRepository { get; }
        IEmployerRepository EmployerRepository { get; }
        IIndustryRepository IndustryRepository { get; }
        IGenderRepository GenderRepository { get; }
        IEducationLevelRepository EducationLevelRepository { get; }
        IJobTypeRepository JobTypeRepository { get; }
        IJobRepository JobRepository { get; }
        IAppRepository AppRepository { get; }
    }
}
    