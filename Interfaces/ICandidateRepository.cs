﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using RegisterModel = Recruitement_App___BEZAO_Test.Areas.Identity.Pages.Account.RegisterModel;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface ICandidateRepository
    {
        void Create(string id, RegisterCandidateViewModel model);
        bool Update(CandidateProfileViewModel model, IFormFile photo = null);
        IEnumerable<Candidate> GetCandidates();
        Candidate GetCandidate(string id);
        void Save();

    }
}