﻿using System.Collections.Generic;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IJobRepository
    {
        string SaveJob(JobViewModel model);
       
        IEnumerable<Job> GetJobs();
        IEnumerable<Industry> GetIndustries();
        Job GetJob(int id);
        Job GetJobFor(int id, string userId);
        IEnumerable<Job> GetJobsFor(string userId);
        void Save();

    }
}