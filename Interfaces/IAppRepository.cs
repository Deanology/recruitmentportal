﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IAppRepository
    {
        string UpSert(ApplicationViewModel model, string userid, IFormFile resume = null);
        
        IEnumerable<Application> GetApplicationsForEmployer(string employerId);
        IEnumerable<Application> GetApplicationsForCandidate(string candidateId);
        Application GetApplicationForCandidate(string userId, int id);
        Application GetApplicationForEmployer(string userId, int id);
       
        void Mark(string userId, bool value, int id);
        void Save();

        IEnumerable<Application> GetApplications();
    }
}