﻿using System.Collections.Generic;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IEducationLevelRepository
    {
        IEnumerable<EducationLevel> GetEducationLevels();
    }
}