﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Controllers
{

    [AutoValidateAntiforgeryToken]
    [Authorize]
    public class CandidateController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IMapper _mapper;

        public CandidateController(
            IUnitOfWork unitOfWork,
            UserManager<IdentityUser> userManager,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }

        [Authorize(Roles = "Candidate")]
        public IActionResult Index()
        {
            ViewBag.Index = "active";
            return View();
        }


        [Authorize(Roles = "Candidate")]
        public IActionResult Profile()
        {
            var userId = _userManager.GetUserId(User);
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.Profile = "active";
            var candidateDb =  _unitOfWork.CandidateRepository.GetCandidate(userId);
            var model = new CandidateProfileViewModel();
            CandidateProfileViewModel vm =  _mapper.Map(candidateDb, model);

            vm.PhoneNumber = user.PhoneNumber;
            vm.Email = user.Email;
            vm.UserName = user.UserName;
            vm.Genders =  _unitOfWork.GenderRepository.GetGenders();
            vm.EducationLevels = _unitOfWork.EducationLevelRepository.GetEducationLevels();
            return View(vm);
        }


        [Authorize(Roles = "Candidate")]
        [HttpPost]
        public ActionResult UpdateProfile(CandidateProfileViewModel model, IFormFile photo = null)
        {
           
            if ( _unitOfWork.CandidateRepository.Update(model, photo))
            {
                TempData["canMsg"] = "Profile Successfully Updated";

                return RedirectToAction("Profile");
            }

            model.Genders = _unitOfWork.GenderRepository.GetGenders();
            model.EducationLevels = _unitOfWork.EducationLevelRepository.GetEducationLevels();
            if (!ModelState.IsValid)
            {
                return View("Profile", model);
            }
            ModelState.AddModelError(string.Empty, "An Internal Error Occured");
            return View("Profile", model);
        }



        [Authorize(Roles = "Candidate")]
        public IActionResult AppliedJob()
        {
            var userId = _userManager.GetUserId(User);
            var model = _unitOfWork.AppRepository.GetApplicationsForCandidate(userId);
            return View(model);
        }

        [Authorize]
        public IActionResult Detail(string id)
        {
            var user = _userManager.FindByIdAsync(id).Result;
            if (user == null) return NotFound();
            var model = new CandidateDetailViewModel
            {
                Candidate =  _unitOfWork.CandidateRepository.GetCandidate(user.Id),
                IdentityUser = user
            };
            

            return View(model);
        }
        [AllowAnonymous]
        public FileContentResult GetMedia(string id)
        {
            var candidate =  _unitOfWork.CandidateRepository.GetCandidate(id);
            return candidate != null ? File(candidate.Photo.Content, candidate.Photo.ContentType) : null;
        }
       


    }
}