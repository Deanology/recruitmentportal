﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Controllers
{
  [AutoValidateAntiforgeryToken]
  [Authorize]
    public class EmployerController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IEmailSender _emailSender;
        private readonly IMapper _mapper;


        public EmployerController(
            UserManager<IdentityUser> userManager, 
            IUnitOfWork unitOfWork,
            IEmailSender emailSender,
            IMapper mapper)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
            _mapper = mapper;
        }

       
        // Employer Dashboard
        [Authorize(Roles = "Employer")]
        public IActionResult Index()
        {
            ViewBag.Active = "active";
            return View();
        }

        // Employer Profile
        [Authorize(Roles = "Employer")]
        public IActionResult Profile()
        {
            var userId = _userManager.GetUserId(User);
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.Profile = "active";
            var employerDb = _unitOfWork.EmployerRepository.GetEmployer(userId);
            EmployerProfileViewModel vm = _mapper.Map(employerDb, new EmployerProfileViewModel());
            vm.PhoneNumber = user.PhoneNumber;
            vm.Email = user.Email;
            vm.UserName = user.UserName;
            vm.Industries = _unitOfWork.IndustryRepository.GetIndustries();
            return View(vm);

        }


        // Employer Update Profile
        [Authorize(Roles = "Employer")]
        [HttpPost]
        public ActionResult UpdateProfile(EmployerProfileViewModel model, IFormFile logo = null)
        {

            if (_unitOfWork.EmployerRepository.Update(model, logo))
            {
                TempData["empMsg"] = "Profile Successfully Updated";

                return RedirectToAction("Profile");
            }

            model.Industries = _unitOfWork.IndustryRepository.GetIndustries();
           
            if (!ModelState.IsValid)
            {
                return View("Profile", model);
            }
            ModelState.AddModelError(string.Empty, "An Internal Error Occured");
            return View("Profile", model);
        }


        // Employer Create New Job
        [Authorize(Roles = "Employer")]
        public ActionResult PostJob()
        {
            var userIdentity = _userManager.GetUserAsync(User).Result;
            var profile = _unitOfWork.EmployerRepository.GetEmployer(userIdentity.Id);

            // Check If Employer Profile is Complete or Updated
            if (profile.Logo == null)
            {
                TempData["empMsg"] = "Complete Your Profile To continue";
                return RedirectToAction("Profile", "Employer");
            }
            var model = new JobViewModel
            {
                EmployerId = _userManager.GetUserId(User),
                JobTypes = _unitOfWork.JobTypeRepository.GetJobTypes()
            };

            return View(model);
        }


        // Employer Edit Job
        [Authorize(Roles = "Employer")]
        public ActionResult EditJob(int id)
        {
            var jobDb = _unitOfWork.JobRepository.GetJob(id);
            // Check  Employer Against Editing other Employer's Job 
            if (jobDb == null ||  jobDb.EmployerId != _userManager.GetUserId(User))
            {
                return NotFound();
            }

            var model = _mapper.Map(jobDb, new JobViewModel());
            model.JobTypes = _unitOfWork.JobTypeRepository.GetJobTypes();
            

            return View("PostJob", model);
        }

        // Upsert Job Action
        [Authorize(Roles = "Employer")]
        [HttpPost]
        public ActionResult Save(JobViewModel model)
        {

            model.JobTypes = _unitOfWork.JobTypeRepository.GetJobTypes();

            if (!ModelState.IsValid)
            {
                return View("PostJob", model);
            }

            var save = _unitOfWork.JobRepository.SaveJob(model);
            switch (save)
            {
                case "Created":
                    TempData["jobMsg"] = "Job Successfully";
                    return RedirectToAction("PostJob");
                case "Updated":
                    TempData["jobMsg"] = "Updated Successfully";
                    return RedirectToAction("PostJob");
                default:
                    ModelState.AddModelError(string.Empty, "An Internal Error Occured");
                    model.EmployerId = _userManager.GetUserId(User);
                    model.JobTypes = _unitOfWork.JobTypeRepository.GetJobTypes();
                    return View("PostJob", model);

            }


        }


        // Candidate Job Application Form 

        [Authorize(Roles = "Candidate")]
        public ActionResult Application(int id)
        {
            // Get Current User
            var userIdentity = _userManager.GetUserAsync(User).Result;

            var profile = _unitOfWork.CandidateRepository.GetCandidate(userIdentity.Id);
            // Check If profile is Updated
            if (profile.PhotoId == null)
            {
                TempData["canMsg"] = "Complete Your Profile To continue";
                return RedirectToAction("Profile", "Candidate");
            }


            // Check If Candidate Already Applied 
            var appformApplied = _unitOfWork.AppRepository.GetApplications()
                .SingleOrDefault(c => c.JobId == id && c.CandidateId == userIdentity.Id);

            if (appformApplied != null)
            {
                TempData["appMsg"] = $"You Have Already Applied for {appformApplied.Job.Title}";
                return RedirectToAction("AppliedJob", "Candidate");
            }

            // Check If Form Exists
            var appformCheck = _unitOfWork.JobRepository.GetJob(id);
            if (appformCheck == null)
            {
                return NotFound();
            }

            // Get other Info About Candidate From Application Db Context
            var appUser = _unitOfWork.CandidateRepository.GetCandidate(userIdentity.Id);
            
            var model = new ApplicationViewModel()
            {
               
                JobId = id,
                PhoneNumber = userIdentity.PhoneNumber,
                FullName = appUser.Fullname,
                CandidateId = appUser.CandidateId,

            };
            return View("AppForm", model);
        }


        // Candidate Review Job Application Form
        [Authorize(Roles = "Candidate")]
        public ActionResult ReviewAppForm(int id)
        {
            // Get Current User
            var userIdentity = _userManager.GetUserAsync(User).Result;


            var appform =  _unitOfWork.AppRepository.GetApplicationForCandidate(userIdentity.Id, id);

            // Check If Application Form Exists
            if (appform == null)
            {
                return NotFound();
            }

            // Check If Application  is Rejected  or Accepted
            if (appform.Approve != null)
            {
                TempData["appMsg"] = "You can no Longer Edit this Application";
                return RedirectToAction("AppliedJob", "Candidate");
            }

            var model = new ApplicationViewModel
            {
                AppId = appform.Id,
                JobId = appform.JobId,
                PhoneNumber = appform.PhoneNumber,
                FullName = appform.FullName,
                Resume = appform.Resume,
                CandidateId = appform.CandidateId,
            };

            ViewBag.Edit = "Review Application";
            TempData["Edit"] = "Edit";
            return View("AppForm", model);
        }


        // Upsert Candidate Job Application Form 
        [Authorize(Roles = "Candidate")]
        [HttpPost]
        public async Task<ActionResult> Submit(ApplicationViewModel model, IFormFile resume =null)
        {


            if (TempData["Edit"] != null)
            {

                ModelState.Remove("TermsConditions");
               
            }
            var user = await _userManager.GetUserAsync(User);
            var employerId = _unitOfWork.JobRepository.GetJob(model.JobId).EmployerId;
            var employer = await _userManager.FindByIdAsync(employerId);
            
            if (!ModelState.IsValid)
            {
                return View("AppForm", model);
            }

            
            var save =  _unitOfWork.AppRepository.UpSert(model,user.Id,resume);

            switch (save)
            {
                case "Created":

                    //Email Will be sent To Employer

                    try
                    {
                        await _emailSender.SendEmailAsync(employer.Email, $"New Application For {_unitOfWork.JobRepository.GetJob(model.JobId).Title}",
                            $"{model.FullName}, Sent An Application  for {_unitOfWork.JobRepository.GetJob(model.JobId).Title}");

                    }
                    catch (Exception e)
                    {
                        TempData["appMsg"] = $"{e.Message} Email Was not Sent Check your Internet or Mail Service";
                        return RedirectToAction("Index", "Home");
                    }

                    
                    TempData["appMsg"] = "Your Application Was Successfully, and A Mail has been Sent to the HR Team";

                    return RedirectToAction("Index", "Home");
                case "Updated":
                    TempData["appMsg"] = "Application Updated Successfully";

                    return RedirectToAction("Index", "Home");
                default:
                    ModelState.AddModelError(string.Empty, "An Internal Error Occured");
                   
                    return View("AppForm", model);

            }
            
        }

        // Applications Sent to Employer 
        [Authorize(Roles = "Employer")]
        public IActionResult Applications()
        {
            var user = _userManager.GetUserAsync(User).Result;
            var model =  _unitOfWork.AppRepository.GetApplicationsForEmployer(user.Id).Where( a => a.Approve == null);

            return View(model);
        }

        // List of Shortlisted Candidate 
        [Authorize(Roles = "Employer")]
        public IActionResult ShortListed()
        {
            var user = _userManager.GetUserAsync(User).Result;
            var model =  _unitOfWork.AppRepository.GetApplicationsForEmployer(user.Id).Where(a => a.Approve == true);
            ViewBag.Approved = true;
            return View("Applications",model);
        }


        // Employer Approve Action
        [Authorize(Roles = "Employer")]
        public async Task<IActionResult> ApproveApplication(int id)
        {
            // Fetch Current User
            var user = await _userManager.GetUserAsync(User);

            var application =  _unitOfWork.AppRepository.GetApplicationForEmployer(user.Id, id);

            // Check if Application Exist
            if (application == null)
            {
                return NotFound();
            }

            // Get Id of Candidate
            var candidate = await _userManager.FindByIdAsync(application.CandidateId);

             _unitOfWork.AppRepository.Mark(user.Id, true, application.Id);

            //Mail to be sent candidate
            try
            {
                await _emailSender.SendEmailAsync(candidate.Email, $"{application.Job.Title} Job Application Approved",
                    $"Congratulations {application.FullName}, You Have been Shortlisted for the Position of {application.Job.Title}"
                );
            }
            catch (Exception e)
            {
                TempData["appMsg"] = $"{e.Message} Email Was not Sent Check your Internet or Mail Service";
                return RedirectToAction("Applications");
            }

            TempData["appMsg"] = "An Acceptance Email has Been Sent to the Candidate";
            return RedirectToAction("Applications");
        }


        // Employer Reject Action
        [Authorize(Roles = "Employer")]
        public async Task<IActionResult> RejectApplication(int id)
        {

            // Fetch Current User
            var user = await _userManager.GetUserAsync(User);
            var application =  _unitOfWork.AppRepository.GetApplicationForEmployer(user.Id, id);

            // Check If Application Exist
            if (application == null)
            {
                return NotFound();
            }

            // Get Id of Candidate
            var candidate = await _userManager.FindByIdAsync(application.CandidateId);
           
             _unitOfWork.AppRepository.Mark(user.Id, false, application.Id);


            //Mail to be sent candidate
            try
            {
                await _emailSender.SendEmailAsync(candidate.Email, $"{application.Job.Title} Job Application Reject",
                    $"{application.FullName}, After Reviewing  Your Application, You currently do not meet up to the job requirements for {application.Job.Title}"
                );
            }
            catch (Exception e)
            {
                TempData["appMsg"] = $"{e.Message} Email Was not Sent Check your Internet or Mail Service";
                return RedirectToAction("Applications");
            }

            TempData["appMsg"] = "A Rejection Email has Been Sent to the Candidate";
            return RedirectToAction("Applications");
        }



        // Employer ManageJobs Action
        [Authorize(Roles = "Employer")]
        public IActionResult ManageJobs()
        {
            var user = _userManager.GetUserAsync(User).Result;
            var model = _unitOfWork.JobRepository.GetJobsFor(user.Id);
            return View(model);
        }


        // Employer Logo Url
        [AllowAnonymous]
        public FileContentResult GetMedia(string id)
        {
            var employer =_unitOfWork.EmployerRepository.GetEmployer(id);
            return employer != null ? File(employer.Logo.Content, employer.Logo.ContentType) : null;
        }



        // Candidate Cv Url
        [Authorize]
        public FileContentResult GetAppDoc(int id)
        {
            var user = _userManager.GetUserAsync(User).Result;
            var application =  _unitOfWork.AppRepository.GetApplicationForEmployer(user.Id, id);
            return application != null ? File(application.Resume.Content, application.Resume.ContentType) : null;
        }
    }
}