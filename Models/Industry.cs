namespace Recruitement_App___BEZAO_Test.Models
{
    public class Industry
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Icon { get; set; }
        public Audit Audit { get; set; }
    }

}