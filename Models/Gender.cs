using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class Gender
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Audit Audit { get; set; }
       
    }
}
