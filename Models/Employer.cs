using System;
using System.Collections.Generic;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class Employer
    {
        public string EmployerId { get; set; }
        public string CompanyName { get; set; }
    
        public DateTime? Founded { get; set; }

        public string Bio { get; set; }

        public string Website { get; set; }
        public int IndustryId { get; set; }
        public Industry Industry { get; set; }
        public File Logo { get; set; }
        public int Capacity { get; set; }

        public Address Address { get; set; }
        public SocialAccount SocialAccount { get; set; }

        public List<Job> Jobs { get; set; }

        public bool? TermsCondition { get; set; }

        public Audit Audit { get; set; }
      
    }
}