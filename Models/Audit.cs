using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Recruitement_App___BEZAO_Test.Models
{
    
  
    public class Audit
    {

        public Audit()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        public DateTime UpdatedAt { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}