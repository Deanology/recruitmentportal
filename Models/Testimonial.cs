namespace Recruitement_App___BEZAO_Test.Models
{
    public class Testimonial
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string Message { get; set; }

        public Audit Audit { get; set; }
    }
}