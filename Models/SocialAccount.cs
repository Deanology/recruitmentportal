using System.ComponentModel.DataAnnotations.Schema;

namespace Recruitement_App___BEZAO_Test.Models
{
  
    public class SocialAccount
    {
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string FaceBook { get; set; }
        public string Instagram { get; set; }
    }
}