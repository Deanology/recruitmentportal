﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class ApplicationDbContext: DbContext
    {

        public DbSet<Gender> Genders { get; set; }
        public DbSet<JobType> JobTypes { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Employer> Employers { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<BookMark> BookMarks { get; set; }

        public DbSet<Testimonial> Testimonials { get; set; }

        public DbSet<ActivityLog> ActivityLogs { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> opts) 
            :base(opts) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Employer>()
                .HasKey(e => e.EmployerId);
            modelBuilder.Entity<Employer>()
                .Property(e => e.EmployerId)
                .ValueGeneratedNever();


            modelBuilder.Entity<Candidate>()
                .HasKey(c => c.CandidateId);
            modelBuilder.Entity<Candidate>()
                .Property(c => c.CandidateId)
                .ValueGeneratedNever();

            modelBuilder.Entity<Employer>( )
                .OwnsOne( e => e.Address);
            modelBuilder.Entity<Employer>()
                .OwnsOne(e => e.SocialAccount); 
            modelBuilder.Entity<Employer>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Candidate>()
                .OwnsOne(e => e.Address);
            modelBuilder.Entity<Candidate>()
                .OwnsOne(e => e.SocialAccount);
            modelBuilder.Entity<Candidate>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Job>()
                .OwnsOne(e => e.Address);

            modelBuilder.Entity<Job>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Gender>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Industry>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Testimonial>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<File>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<BookMark>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<JobType>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<EducationLevel>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<Application>()
                .OwnsOne(e => e.Audit);

            modelBuilder.Entity<ActivityLog>()
                .OwnsOne(e => e.Audit);

            base.OnModelCreating(modelBuilder);
        }
    }
}
