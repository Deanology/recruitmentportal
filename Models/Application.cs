namespace Recruitement_App___BEZAO_Test.Models
{
    public class Application
    {
        public int Id { get; set; }
        public string CandidateId { get; set; }

        public int JobId { get; set; }
        public Job Job { get; set; }
        public Candidate Candidate { get; set; }

        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public File Resume { get; set; }

        public bool? Approve { get; set; }
        public bool? TermsConditions { get; set; }
        public Audit Audit { get; set; }
    }
}