﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class CandidateProfileViewModel
    {
        
        public string CandidateId { get; set; }

        [BindNever]
        public string Fullname { get; set; }


        [BindNever]
        public string Email { get; set; }

        [BindNever]
        public string UserName { get; set; }


        [BindNever]
        public int GenderId { get; set; }

        public IEnumerable<Gender> Genders { get; set; }

        [Required(ErrorMessage = "Enter Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }
        

        [BindNever]
        public string PhoneNumber { get; set; }
        public string Bio { get; set; }

        [Required]
        public string Languages { get; set; }

        [Required(ErrorMessage = "Residence cant be blank")]
        public Address Address { get; set; }

        public File Photo { get; set; }

        [Required(ErrorMessage = "Select an Level")]

        [Display(Name = "Education Levels")]

        public int EducationLevelId { get; set; }
        public IEnumerable<EducationLevel>  EducationLevels { get; set; }

        [Display(Name = "Social Accounts")]
        public SocialAccount SocialAccount { get; set; }
    }
}
