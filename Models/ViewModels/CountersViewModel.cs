﻿namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class CountersViewModel
    {
        public int Companies { get; set; }
        public int Applications { get; set; }
        public int Candidates { get; set; }
        public int Jobs { get; set; }
    }
}
