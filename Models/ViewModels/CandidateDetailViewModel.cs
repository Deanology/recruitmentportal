﻿using Microsoft.AspNetCore.Identity;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class CandidateDetailViewModel
    {
        public IdentityUser IdentityUser { get; set; }
        public Candidate Candidate { get; set; }
    }
}
