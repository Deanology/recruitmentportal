﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class RegisterEmployerViewModel
    {
        [Required] [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Capacity")]
        [Range(20, int.MaxValue)]
        public int Capacity { get; set; }

        [Required(ErrorMessage = "Select An Industry")]
        [Display(Name = "Industry")]
        public int IndustryId { get; set; }
        public IEnumerable<Industry> Industries { get; set; } = new List<Industry>();

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
            MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "You have not Accepted Our Terms And Conditions!")]
        [Display(Name = "Terms&Conditions")]
        public bool TermsConditions { get; set; }
    }
}