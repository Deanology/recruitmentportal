﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class RegisterCandidateViewModel
    {

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }
        public IEnumerable<Gender> Genders { get; set; } = new List<Gender>();

        [Required]
        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }



        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        
        [Range(typeof(bool), "true", "true", ErrorMessage = "You have not Accepted Our Terms And Conditions!")]
        [Display(Name = "Terms&Conditions")]
        public bool TermsConditions { get; set; }

    }
}
