using System;
using System.Collections.Generic;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class Candidate
    {
        public string CandidateId { get; set; }
        public string Fullname { get; set; }

        public int GenderId { get; set; }
        public Gender Gender { get; set; }

        public DateTime? DOB { get; set; }

        public string Bio { get; set; }

        public int?  PhotoId{ get; set; }
        public File Photo { get; set; }
        public string Languages { get; set; }

        public Address Address { get; set; }

        public  int? EducationLevelId  { get; set; }
       
        public EducationLevel EducationLevel { get; set; }
        public SocialAccount SocialAccount { get; set; }
        public bool? TermsCondition { get; set; }

        public List<Application> Applications { get; set; }

        public Audit Audit { get; set; }



    }
}