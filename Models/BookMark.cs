namespace Recruitement_App___BEZAO_Test.Models
{
    public class BookMark
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public Job Job { get; set; }

        public int CandidateId { get; set; }

        public Audit Audit { get; set; }
    }
}