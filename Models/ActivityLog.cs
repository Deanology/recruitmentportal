namespace Recruitement_App___BEZAO_Test.Models
{
    public class ActivityLog
    {
        public int Id  { get; set; }
        public string Action { get; set; }
        public string Name { get; set; }
        public string Object { get; set; }

        public Audit Audit { get; set; }
    }
}