﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Recruitement_App___BEZAO_Test.Migrations.ApplicationDb
{
    public partial class FKJob : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_JobTypes_TypeId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_TypeId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "Jobs");

            migrationBuilder.AddColumn<int>(
                name: "JobTypeId",
                table: "Jobs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_JobTypeId",
                table: "Jobs",
                column: "JobTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_JobTypes_JobTypeId",
                table: "Jobs",
                column: "JobTypeId",
                principalTable: "JobTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_JobTypes_JobTypeId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_JobTypeId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobTypeId",
                table: "Jobs");

            migrationBuilder.AddColumn<int>(
                name: "TypeId",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_TypeId",
                table: "Jobs",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_JobTypes_TypeId",
                table: "Jobs",
                column: "TypeId",
                principalTable: "JobTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
