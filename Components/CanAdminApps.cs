﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class CanAdminApps : ViewComponent
    {
        private readonly IAppRepository _app;
        private readonly UserManager<IdentityUser> _userManager;
        private IdentityUser _identityUser;

        public CanAdminApps(IAppRepository app, UserManager<IdentityUser> userManager)
        {

            _app = app;
            _userManager = userManager;

        }


        public IViewComponentResult Invoke()
        {
            _identityUser = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var model = _app.GetApplicationsForCandidate(_identityUser.Id).Take(5);

            return View(model);
        }
    }
}