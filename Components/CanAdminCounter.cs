﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class CanAdminCounter : ViewComponent
    {
        private readonly IAppRepository _app;
        private readonly UserManager<IdentityUser> _userManager;
        private IdentityUser _identityUser;

        public CanAdminCounter(IAppRepository app, UserManager<IdentityUser> userManager)
        {
            _app = app;
            _userManager = userManager;

        }


        public IViewComponentResult Invoke()
        {
            _identityUser = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var model = _app.GetApplicationsForCandidate(_identityUser.Id);

            return View(model);
        }
    }
}