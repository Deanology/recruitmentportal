﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class EmpAdminCounter : ViewComponent
    {
        private readonly IJobRepository _job;
        private readonly UserManager<IdentityUser> _userManager;
        private IdentityUser _identityUser;

        public EmpAdminCounter(IJobRepository job, UserManager<IdentityUser> userManager)
        {
            _job = job;
            _userManager = userManager;
            
        }

        
        public IViewComponentResult Invoke()
        {
            _identityUser = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var model = _job.GetJobsFor(_identityUser.Id);

            return View(model);
        }
    }
}